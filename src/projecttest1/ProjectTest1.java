/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecttest1;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;
import jssc.SerialPortTimeoutException;
/**
 *
 * @author pinaev
 */
public class ProjectTest1 {

    /**
     * @param args the command line arguments
     */
    static SerialPort serialPort;
    public static void main(String[] args) {
        /**
        //Метод getPortNames() возвращает массив строк. Элементы массива уже отсортированы.
        String[] portNames = SerialPortList.getPortNames();
        System.out.println("All COM Port names:");
        for(int i = 0; i < portNames.length; i++){
            System.out.println(portNames[i]);
        }
        */
        
        serialPort = new SerialPort("COM3");
        try {
            serialPort.openPort();
            serialPort.setParams(SerialPort.BAUDRATE_9600, 
                             SerialPort.DATABITS_8,
                             SerialPort.STOPBITS_1,
                             SerialPort.PARITY_NONE);
            byte [] send = {66, 03, 00, 00, 00, 15, 11, 61};
        //    serialPort.writeString("42 03 00 00 00 0f 0b 3d");
        //                            66 03 00 00 00 15 11 61
        //  byte[] b={(byte)0x31,(byte)0x32,0x7a,(byte)0xff}; - еще один пример заполнения
            serialPort.writeBytes(send);
        
         /**
        //Wait 2000 ms
            try {
                Thread.sleep(1);  //  25 ms и меньше: метод 1 выдает null, метод 2 работает
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
       
        //Метод 1. Чтение полностью, строкой
            String data = serialPort.readString();
            System.out.println(data);
        
            
        //Метод 2. Чтение полностью, байтами из буфера
            System.out.println("InputBuffer = " + serialPort.getInputBufferBytesCount() + "\n" +
                               "OutputBuffer = " + serialPort.getOutputBufferBytesCount()); 
            byte[] buffer;
            try { 
                buffer = serialPort.readBytes(35, 10000); //35 байт с таймаутом 10 сек
                System.out.println(buffer);  //через отладку видим нужные значения.
            } catch (SerialPortTimeoutException ex) {
                Logger.getLogger(ProjectFirst.class.getName()).log(Level.SEVERE, null, ex);
            }
        */
          
        //Метод 2. Чтение данных по событию
            int mask = SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR;//Prepare mask
            serialPort.setEventsMask(mask);//Set mask
            serialPort.addEventListener(new SerialPortReader());//Add SerialPortEventListener
          
        //serialPort.closePort();   
        }
        catch (SerialPortException ex) {
            System.out.println(ex);
        }
    }

    static class SerialPortReader implements SerialPortEventListener {

    public void serialEvent(SerialPortEvent event) { 
        if(event.isRXCHAR()){//If data is available
            if(event.getEventValue() == 35){//Check bytes count in the input buffer
                //Read data, if 35 bytes available 
                try {
                    //String buffer = serialPort.readString(event.getEventValue());
                    //byte buffer[] = serialPort.readBytes(event.getEventValue());
                    //Разные способы прочтения
                    byte buffer[] = serialPort.readBytes(35);
                    for(int i = 0; i < buffer.length; i++)
                    {
                        System.out.print(buffer[i] + " ");
                    }
                }
                catch (SerialPortException ex) {
                    System.out.println(ex);
                }
            }
        }
        /** Разобраться, для чего можно использовать
        else if(event.isCTS()){//If CTS line has changed state
            if(event.getEventValue() == 1){//If line is ON
                System.out.println("CTS - ON");
            }
            else {
                System.out.println("CTS - OFF");
            }
        }
        else if(event.isDSR()){///If DSR line has changed state
            if(event.getEventValue() == 1){//If line is ON
                System.out.println("DSR - ON");
            }
            else {
                System.out.println("DSR - OFF");
            }
        }
        */
    }
}
    
}
